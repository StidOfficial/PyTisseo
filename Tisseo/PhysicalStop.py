class PhysicalStop:
	def __init__(self, destinations=[], handicappedCompliance=False, id=-1, lines=[], name="", operatorCodes=[], stopArea=None):
		self.destinations = destinations
		self.handicappedCompliance = handicappedCompliance
		self.id = id
		self.lines = lines
		self.name = name
		self.operatorCodes = operatorCodes
		self.stopArea = stopArea

	def __str__(self):
		return ("{{destination: {}, handicappedCompliance, {}, id: {}, lines: {}, name: \"{}\", operatorCodes: {}, stopArea: {}}}").format(self.destinations, self.handicappedCompliance, self.id, self.lines, self.name, self.operatorCodes, self.stopArea)
