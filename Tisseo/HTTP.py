from enum import Enum
import requests
import datetime

class HTTP:
	class Format(Enum):
		JSON = 1
		XML = 2

	@staticmethod
	def getFormatName(format):
		if(format == HTTP.Format.JSON):
			return "json"
		elif(format == HTTP.Format.XML):
			return "xml"
		else:
			return "json"

	def __init__(self, protocol="https", server_address="api.tisseo.fr", version=None, key=None):
		if key == None:
			raise ValueError("API key is not set")
		self.base_url = "{}://{}".format(protocol, server_address)
		if version == None:
			response = requests.get(self.base_url)
			if response.status_code == 200:
				for version in response.json()["versions"]:
					if version["status"] == "current":
						version = version["value"]

		self.api_url = "{}/{}".format(self.base_url, version)
		self.key = key

	def request(self, service_name, format=None, params={}):
		url = "{}/{}.{}".format(self.api_url, service_name, self.getFormatName(format))

		final_params = params
		final_params.update({"key": self.key})
		http_request = requests.get(url, params=params)
		if http_request.status_code == 200:
			requestDateGMT = datetime.datetime.strptime(http_request.headers["Date"], "%a, %d %b %Y %H:%M:%S %Z")
			requestDateUTC = requestDateGMT.replace(tzinfo=datetime.timezone.utc)

			response = {"serverDate": requestDateUTC.astimezone()}

			if format == None or format == HTTP.Format.JSON:
				response.update({"content": http_request.json()})
			elif format == TisseoAPI.HTTP.Format.XML:
				response.update({"content": http_request.content})

			return response
		else:
			raise ValueError("API request failed")
