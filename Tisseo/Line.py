class Line:
	def __init__(self, bgXmlColor="#FFFFFF", color="(255,255,255)", fgXmlColor="#FFFFFF", id=-1, name="", network="",
			reservationMandatory=0, shortName="", transportMode=None, terminus=[],
			messages=[], geometry=None, style=""):
		self.bgXmlColor = bgXmlColor
		self.color = color
		self.fgXmlColor = fgXmlColor
		self.id = id
		self.name = name
		self.network = network
		self.reservationMandatory = reservationMandatory
		self.shortName = shortName
		self.transportMode = transportMode
		self.terminus = terminus
		self.messages = messages
		self.geometry = geometry
		self.style = style

	def __str__(self):
		return ("{{bgXmlColor: \"{}\", color: \"{}\", fgXmlColor: \"{}\", id: {}, "
			"name: \"{}\", network: \"{}\", reservationMandatory: {}, shortName: \"{}\", "
			"transportMode: {}, terminus: {}, messages: {}, geometry: {}, style: \"{}\"}}").format(
				self.bgXmlColor, self.color, self.fgXmlColor, self.id, self.name,
				self.network, self.reservationMandatory, self.shortName,
				self.transportMode, self.terminus, self.messages, self.geometry, self.style)
