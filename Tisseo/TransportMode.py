class TransportMode:
	def __init__(self, article, id, name):
		self.article = article
		self.id = id
		self.name = name

	def getShortName(self):
		if self.name == "métro":
			return "M"
		elif self.name == "bus":
			return "B"
		elif self.name == "tramway":
			return "T"
		elif self.name == "transport à la demande":
			return "TAD"
		else:
			return "-"

	def __str__(self):
		return ("{{article: \"{}\", id: {}, name: \"{}\"}}").format(self.article, self.id, self.name)
