class Departures:
	def __init__(self, departures=[], stop=None, stopArea=None, stopAreas=None):
		self.departures = departures
		self.stop = stop
		self.stopArea = stopArea
		self.stopAreas = stopAreas

	def __str__(self):
		return "{{departures: {}, stop: {}, stopArea: {}, stopAreas: {}}}".format(self.departures, self.stop, self.stopArea, self.stopAreas)
