class Terminus:
	def __init__(self, id, cityName, name):
		self.id = id
		self.cityName = cityName
		self.name = name

	def __str__(self):
		return ("{{id: {}, cityName: \"{}\", name: \"{}\"}}").format(self.id, self.cityName,
				self.name)
