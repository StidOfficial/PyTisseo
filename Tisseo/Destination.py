class Destination:
	def __init__(self, cityName="", id=-1, line=None, name=""):
		self.cityName = cityName
		self.id = id
		self.line = line
		self.name = name

	def __str__(self):
		return "{{cityName: \"{}\", id: {}, lines: {}, name: \"{}\"}}".format(self.cityName, self.id, self.line, self.name)
