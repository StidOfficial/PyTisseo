class Place:
	def __init__(self, category="", className="", id=-1, key="", label="", network="",rank=-1,
			x=0.0, y=0.0, cityName="", postcode="", address="", typeCompressed="", type="",
			veloStation=0):
		self.category = category
		self.className = className
		self.id = id
		self.key = key
		self.label = label
		self.network = network
		self.rank = rank
		self.x = x
		self.y = y
		self.cityName = cityName
		self.postcode = postcode
		self.address = address
		self.typeCompressed = typeCompressed
		self.type = type
		self.veloStation = veloStation

	def __str__(self):
		return ("{{category: \"{}\", className: \"{}\", id: {}, key: \"{}\", label: \"{}\", "
			"network: \"{}\", rank: {}, x: {}, y: {}, cityName: \"{}\", postcode: \"{}\", "
			"address: \"{}\", typeCompressed: \"{}\", type: \"{}\", veloStation: {}}}"
			).format(self.category, self.className, self.id, self.key, self.label,
				self.network, self.rank, self.x, self.y, self.cityName, self.postcode,
				self.address, self.typeCompressed, self.type, self.veloStation)
