class Departure:
	def __init__(self, dateTime="", destination=None, line=None, realTime=False):
		self.dateTime = dateTime
		self.destination = destination
		self.line = line
		self.realTime = realTime

	def __str__(self):
		return "{{dateTime: \"{}\", destination: {}, line: {}, realTime: {}}}".format(self.dateTime, self.destination, self.line, self.realTime)
