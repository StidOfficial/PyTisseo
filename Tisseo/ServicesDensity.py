class ServicesDensity:
	def __init__(self, distance=0, isServiceNumberReached=False, serviceNumberReached=0, stops=[]):
		self.distance = distance
		self.isServiceNumberReached = isServiceNumberReached
		self.serviceNumberReached = serviceNumberReached
		self.stops = stops

	def __str__(self):
		return "{{distance: {}, isServiceNumberReached: {}, serviceNumberReached: {}, stops: {}}}".format(self.distance, self.isServiceNumberReached, self.serviceNumberReached, self.stops)
