class Stop:
	def __init__(self, dataSource="", distance=0, id=-1, name="", operatorCode=[], handicappedCompliance=False):
		self.dataSource = dataSource
		self.distance = distance
		self.id = id
		self.name = name
		self.operatorCode = operatorCode
		self.handicappedCompliance = handicappedCompliance

	def __str__(self):
		return "{{dataSource: \"{}\", distance: {}, id: {}, name: \"{}\", operatorCode: {}, handicappedCompliance: {}}}".format(self.dataSource, self.distance, self.id, self.name, self.operatorCode, self.handicappedCompliance)
