class StopArea:
	def __init__(self, cityId=-1, cityName="", id=-1, name="", x=0, y=0, schedules=None, uniqueStopId=-1):
		self.cityId = cityId
		self.cityName = cityName
		self.id = id
		self.name = name
		self.x = x
		self.y = y
		self.schedules = schedules
		self.uniqueStopId = uniqueStopId

	def __str__(self):
		return "{{cityId: {}, cityName: \"{}\", id: {}, name: \"{}\", x: {}, y: {}, schedules: {}, uniqueStopId: {}}}".format(self.cityId, self.cityName, self.id, self.name, self.x, self.y, self.schedules, self.uniqueStopId)
