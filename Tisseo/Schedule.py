class Schedule:
	def __init__(self, destinations=[], destination=None, journeys=[], line=None, stop=None):
		self.destinations = destinations
		self.destination = destination
		self.journeys = journeys
		self.line = line
		self.stop = stop

	def __str__(self):
		return "{{destination: {}, destination: {}, journeys: {}, line: {}, stop: {}}}".format(self.destinations, self.destination, self.journeys, self.line, self.top)
