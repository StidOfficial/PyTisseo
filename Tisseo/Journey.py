import datetime

class Journey:
	def __init__(self, dateTime=None, realTime=False, waitingTime=None, realDateTime=None):
		self.dateTime = dateTime
		self.realTime = realTime
		self.waitingTime = waitingTime
		self.realDateTime = realDateTime

	def getWaitingTime(self):
		if self.realDateTime != None:
			dateTime = self.realDateTime
		else:
			dateTime = self.dateTime

		if dateTime < datetime.datetime.now():
			return datetime.time(0, 0, 0)

		datetimeDiff = dateTime - datetime.datetime.now()

		hours = divmod(datetimeDiff.seconds, 3600)
		minutes = divmod(hours[1], 60)
		seconds = minutes[1]

		return datetime.time(hours[0], minutes[0], seconds)

	def __str__(self):
		return "{{dateTime: {}, realTime: {}, waitingTime: {}, realDateTime: {}}}".format(self.dateTime, self.realTime, self.waitingTime, self.realDateTime)
