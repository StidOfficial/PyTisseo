class OperatorCode:
	def __init__(self, network="", value=0):
		self.network = network
		self.value = value

	def __str__(self):
		return "{{network: \"{}\", value: {}}}".format(self.network, self.value)
