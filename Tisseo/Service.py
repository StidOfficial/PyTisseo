class Service:
	def __init__(self, JourneyName="", LineName="", departure="", id=0, network="", rollingStock=""):
		self.JournetName = JournetName
		self.LineName = LineName
		self.departure = departure
		self.id = id
		self.network = network
		self.rollingStock = rollingStock

	def __str__(self):
		return "{{JournetName: \"{}\", LineName: \"{}\", departure: \"{}\", id: {}, network: \"{}\", rollingStock: \"{}\"}}".format(self.JournetName, self.LineName, self.departure, self.id, self.network, self.rollingStock)
