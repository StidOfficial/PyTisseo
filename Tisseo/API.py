from Tisseo import HTTP
from Tisseo import Network
from Tisseo import Line
from Tisseo import TransportMode
from Tisseo import Terminus
from Tisseo import Message
from Tisseo import RollingStock
from Tisseo import Place
from Tisseo import StopArea
from Tisseo import PhysicalStop
from Tisseo import Destination
from Tisseo import OperatorCode
from Tisseo import ServicesDensity
from Tisseo import Service
from Tisseo import Stop
from Tisseo import Departure
from Tisseo import Departures
from Tisseo import Journey
from Tisseo import Schedule

import datetime
from hashlib import md5

class API:
	@staticmethod
	def validateParams(params):
		for key in list(params):
			value = params[key]

			if value == None:
				del params[key]
			elif isinstance(value, bool):
				params[key] = int(value)
			elif value == "yes":
				params[key] = True
			elif value == "no":
				params[key] = False

		return params

	@staticmethod
	def isValidContentFormat(contentFormat):
		return contentFormat == "text" or contentFormat == "html"

	@staticmethod
	def isValidBBOX(bbox):
		return isinstance(bbox, list) and len(bbox) == 4

	@staticmethod
	def isValidXY(xy):
		return isinstance(xy, list) and len(xy) == 2

	@staticmethod
	def toIntArray(str):
		strArray = str.split(",")
		for i in range(len(strArray)):
			strArray[i] = int(strArray[i])

		return strArray

	@staticmethod
	def toTime(str):
		timeList = str.split(":")
		return (datetime.datetime.min + datetime.timedelta(hours=int(timeList[0]), minutes=int(timeList[1]), seconds=int(timeList[2]))).time()

	@staticmethod
	def toDateTime(str):
		return datetime.datetime.strptime(str, "%Y-%m-%d %H:%M:%S")

	@staticmethod
	def toTimeDelta(time):
		return datetime.datetime.combine(datetime.date.min, time) - datetime.datetime.min

	def __init__(self, protocol="https", server_address="api.tisseo.fr", version=None, key=None):
		self.http = HTTP.HTTP(protocol=protocol, server_address=server_address, version=version, key=key)
		self.cache = {}

	def request(self, service_name, format=None, params={}):
		params_checksum = md5(str(params).encode()).hexdigest()
		cache_content = self.cache.get(params_checksum)

		if cache_content == None or cache_content.get("expirationDate") < datetime.datetime.now():
			http_response = self.http.request(service_name=service_name, format=format, params=params)

			if format == None or format == HTTP.Format.JSON:
				if len(http_response["content"]) == 0:
					raise ValueError("Invalid response")

				expirationDate = datetime.datetime.strptime(http_response["content"].get("expirationDate"), "%Y-%m-%d %H:%M")
				self.cache.update({params_checksum: {
					"expirationDate": expirationDate,
					"response": http_response
				}})
				return http_response
			elif format == HTTP.Format.XML:
				print("XML")
		else:
			return cache_content.get("response")


	def getStopArea(self, network="Tisséo", srid=4326, bbox=None, displayLines=False, displayCoordXY=False, lineId=None, terminusId=None, timeframe=True, ignoreUnservedStops=False, displayArrivalOnlyLines=False, displayStopPoints=False, name=None, id=None):
		if name == None and id == None:
			return None

		stopAreas = self.getStopAreas(network, srid, bbox, displayLines, displayCoordXY, lineId, terminusId, timeframe, ignoreUnservedStops, displayArrivalOnlyLines, displayStopPoints)

		for stopArea in stopAreas:
			if name != None and stopArea.name == name:
				return stopArea
			elif id != None and stopArea.id == id:
				return stopArea

		return None


	def getStopAreas(self, network="Tisséo", srid=4326, bbox=None, displayLines=False, displayCoordXY=False, lineId=None, terminusId=None, timeframe=True, ignoreUnservedStops=False, displayArrivalOnlyLines=False, displayStopPoints=False):
		if bbox != None and not API.isValidBBOX(bbox):
			raise ValueError("Invalid bbox")

		params = {
			"network": network,
			"srid": srid,
			"bbox": bbox if bbox == None else ",".join(map(str, bbox)),
			"displayLines": displayLines,
			"displayCoordXY": displayCoordXY,
			"lineId": lineId,
			"terminusId": terminusId,
			"timeframe": timeframe,
			"ignoreUnservedStops": ignoreUnservedStops,
			"displayArrivalOnlyLines": displayArrivalOnlyLines,
			"displayStopPoints": displayStopPoints
		}

		stopAreas = []
		for stopArea in self.request(service_name = "stop_areas", params = API.validateParams(params))["content"].get("stopAreas").get("stopArea"):
			stopAreas.append(StopArea.StopArea(
				cityName=stopArea.get("cityName"),
				id=stopArea.get("id"),
				name=stopArea.get("name"),
				x=stopArea.get("x"),
				y=stopArea.get("y")
			))

		return stopAreas

	def getStopPoints(self, network="Tisséo", srid=4326, bbox=None, sortByDistance=False, number=None, displayDestinations=False, displayLines=False, displayCoordXY=False, lineId=None, stopAreaId=None, timeFrame=True, displayStopsWithoutDeparture=False):
		if not API.isValidBBOX(bbox):
			raise ValueError("Invalid bbox")

		params = {
			"network": network,
			"srid": srid,
			"bbox": bbox if bbox == None else ",".join(map(str, bbox)),
			"sortByDistance": sortByDistance,
			"number": number,
			"displayDestinations": displayDestinations,
			"displayLines": displayLines,
			"displayCoordXY": displayCoordXY,
			"lineId": lineId,
			"stopAreaId": stopAreaId,
			"timeFrame": timeFrame,
			"displayStopsWithoutDeparture": displayStopsWithoutDeparture
		}

		physicalStops = []
		for physicalStop in self.request(service_name = "stop_points", params = API.validateParams(params))["content"].get("physicalStops").get("physicalStop"):
			print(physicalStop)
			destinations = []
			for destination in physicalStop.get("destinations"):
				if 'line' in destination:
					_line = destination.get("line")[0]
					if 'transportMode' in _line:
						transportMode = TransportMode.TransportMode(
							_line.get("transportMode").get("article"),
							int(_line.get("transportMode").get("id")),
							_line.get("transportMode").get("name"),
						)
					else:
						transportMode = None

					line = Line.Line(
						_line.get("bgXmlColor"),
						_line.get("color"),
						_line.get("fgXmlColor"),
						int(_line.get("id")),
						_line.get("name"),
						_line.get("network"),
						int(_line.get("reservationMandatory")),
						_line.get("shortName"),
						transportMode
					)
				else:
					line = None

				destinations.append(Destination.Destination(
					destination.get("cityName"),
					int(destination.get("id")),
					line,
					destination.get("name")
				))

			lines = []
			for line in physicalStop.get("lines"):
				lines.append(line.get("short_name"))

			operatorCodes = []
			for operatorCode in physicalStop.get("operatorCodes"):
				_operatorCode = operatorCode.get("operatorCode")
				operatorCodes.append(OperatorCode.OperatorCode(
					_operatorCode.get("network"),
					int(_operatorCode.get("value"))
				))

			stopArea = StopArea.StopArea(
				cityName=physicalStop.get("stopArea").get("cityName"),
				id=int(physicalStop.get("stopArea").get("id")),
				name=physicalStop.get("stopArea").get("name")
			)

			physicalStops.append(PhysicalStop.PhysicalStop(
				destinations,
				bool(int(physicalStop.get("handicappedCompliance"))),
				int(physicalStop.get("id")),
				lines,
				physicalStop.get("name"),
				operatorCodes,
				stopArea
			))

		return physicalStops

	def getPlaces(self, term=None, network="Tisséo", coordinatesXY=None, maxDistance=300, srid=4326, bbox=None, number=None, displayBestPlace=False, displayOnlyStopAreas=False, displayOnlyRoads=False, displayOnlyAddresses=False, displayOnlyPublicPlaces=False, displayOnlyCities=False, lang="fr", simple=0, publicPlaceFilter=None):
		if not API.isValidXY(coordinatesXY):
			raise ValueError("Invalid coordinatesXY")

		if not API.isValidBBOX(bbox):
			raise ValueError("Invalid bbox")

		params = {
			"term": term,
			"network": network,
			"coordinatesXY": coordinatesXY if coordinatesXY == None else ",".join(map(str, coordinatesXY)),
			"maxDistance": maxDistance,
			"srid": srid,
			"bbox": bbox if bbox == None else ",".join(map(str, bbox)),
			"number": number,
			"displayBestPlace": displayBestPlace,
			"displayOnlyStopAreas": displayOnlyStopAreas,
			"displayOnlyRoads": displayOnlyRoads,
			"displayOnlyAddresses": displayOnlyAddresses,
			"displayOnlyPublicPlaces": displayOnlyPublicPlaces,
			"displayOnlyCities": displayOnlyCities,
			"lang": lang,
			"simple": simple,
			"publicPlaceFilter": publicPlaceFilter
		}

		places = []
		for place in self.request(service_name = "places", params = API.validateParams(params))["content"].get("placesList").get("place"):
			places.append(Place.Place(
				place.get("category"),
				place.get("className"),
				place.get("id"),
				place.get("key"),
				place.get("label"),
				place.get("network"),
				place.get("rank"),
				place.get("x"),
				place.get("y"),
				place.get("cityName"),
				place.get("postcode"),
				place.get("address"),
				place.get("typeCompressed"),
				place.get("type"),
				place.get("veloStation")
			))

		return places

	def getNetworks(self):
		networks = []
		for network in self.request(service_name = "networks", params={})["content"].get("networks"):
			networks.append(Network.Network(network.get("id"), network.get("name")))

		return networks

	def getLine(self, network=None, lineId=None, shortName=None, displayTerminus=False, displayMessages=False, displayOnlyDisrupted=False, displayGeometry=False, contentFormat="text"):
		lines = self.getLines(network, lineId, shortName, displayTerminus, displayMessages, displayOnlyDisrupted, displayGeometry, contentFormat)
		if len(lines) > 0:
			return lines[0]
		else:
			return None

	def getLines(self, network=None, lineId=None, shortName=None, displayTerminus=False, displayMessages=False, displayOnlyDisrupted=False, displayGeometry=False, contentFormat="text"):
		if not API.isValidContentFormat(contentFormat):
			raise ValueError("Invalid contentFormat")

		params = {
			"network": network,
			"lineId": lineId,
			"shortName": shortName,
			"displayTerminus": int(displayTerminus),
			"displayMessages": int(displayMessages),
			"displayOnlyDisrupted": int(displayOnlyDisrupted),
			"displayGeometry": int(displayGeometry),
			"contentFormat": contentFormat
		}

		lines = []
		for line in self.request(service_name = "lines", params = API.validateParams(params))["content"].get("lines").get("line"):
			if 'transportMode' in line:
				transportMode = TransportMode.TransportMode(line.get("transportMode").get("article"), int(line.get("transportMode").get("id")), line.get("transportMode").get("name"))
			else:
				transportMode = None

			terminus = []
			if 'terminus' in line:
				for _terminus in line.get("terminus"):
					terminus.append(Terminus.Terminus(int(_terminus.get("id")), _terminus.get("cityName"), _terminus.get("name")))

			messages = []
			if 'messages' in line:
				for message in line.get("messages"):
					messages.append(Message.Message(
						message.get("content"),
						message.get("id"),
						message.get("importanceLevel"),
						message.get("scope"),
						message.get("title"),
						message.get("type"),
						message.get("url")
					))

			if 'geometry' in line:
				geometry = line.get("geometry")[0].get("wkt")
			else:
				geometry = None

			lines.append(Line.Line(
				line.get("bgXmlColor"),
				line.get("color"),
				line.get("fgXmlColor"),
				int(line.get("id")),
				line.get("name"),
				line.get("network"),
				int(line.get("reservationMandatory")),
				line.get("shortName"),
				transportMode,
				terminus,
				messages,
				geometry
			))

		return lines

	def getStopsSchedules(self, operatorCode=None, stopPointId=None, stopAreaId=None, stopsList=None, network="Tisséo", number=10, lineId=None, displayRealTime=True, timetableByArea=False, datetime=None, maxDays=7, firstAndLastOfDay=False):
		params = {
			"operatorCode": operatorCode,
			"stopPointId": stopPointId,
			"stopAreaId": stopAreaId,
			"stopsList": stopsList,
			"network": network,
			"number": number,
			"lineId": lineId,
			"displayRealTime": int(displayRealTime),
			"timetableByArea": int(timetableByArea),
			"datetime": datetime,
			"maxDays": maxDays,
			"firstAndLastOfDay": int(firstAndLastOfDay)
		}

		request = self.request(service_name = "stops_schedules", params = params)
		serverDate = request["serverDate"].replace(tzinfo=None)
		_departures = request["content"].get("departures")

		departures = []
		if 'departure' in _departures:
			for departure in _departures.get("departure"):
				if 'destination' in departure:
					destination = Destination.Destination(
						cityName = departure.get("destination")[0].get("cityName"),
						id = int(departure.get("destination")[0].get("id")),
						name = departure.get("destination")[0].get("name")
					)

				if 'line' in departure:
					line = Line.Line(
						color = departure.get("line").get("color"),
						name = departure.get("line").get("name"),
						network = departure.get("line").get("network"),
						shortName = departure.get("line").get("shortName")
					)

				if 'realTime' in departure:
					if departure.get("realTime") == "yes":
						realTime = True
					elif departure.get("realTime") == "no":
						realTime = False
					else:
						realTime = None

				departures.append(Departure.Departure(
					API.toDateTime(departure.get("dateTime")),
					destination,
					line,
					realTime
				))

		if 'stop' in _departures:
			stop = Stop.Stop(
				id=int(_departures.get("stop").get("id")),
				name=_departures.get("stop").get("name"),
				operatorCode=int(_departures.get("stop").get("operatorCode"))
			)
		else:
			stop = None

		if 'stopArea' in _departures:
			stopArea = StopArea.StopArea(
				int(_departures.get("stopArea").get("cityId")),
				_departures.get("stopArea").get("cityName"),
				int(_departures.get("stopArea").get("id")),
				_departures.get("stopArea").get("name"),
			)
		else:
			stopArea = None

		stopAreas = []
		if 'stopAreas' in _departures:
			for stopArea in _departures.get("stopAreas"):
				schedules = []
				for schedule in stopArea.get("schedules"):
					destination = Destination.Destination(
						int(schedule.get("destination").get("cityId")),
						schedule.get("destination").get("cityName"),
						int(schedule.get("destination").get("id")),
						schedule.get("destination").get("name"),
					)

					journeys = []
					for journey in schedule.get("journeys"):
						realTime = bool(int(journey.get("realTime")))
						waiting_time = API.toTime(journey.get("waiting_time"))

						if realTime:
							realDateTime = serverDate + API.toTimeDelta(waiting_time)
						else:
							realDateTime = None

						journeys.append(Journey.Journey(
							API.toDateTime(journey.get("dateTime")),
							realTime,
							waiting_time,
							realDateTime
						))

					line = Line.Line(
						bgXmlColor=schedule.get("line").get("bgXmlColor"),
						color=schedule.get("line").get("color"),
						fgXmlColor=schedule.get("line").get("fgXmlColor"),
						id=int(schedule.get("line").get("id")),
						name=schedule.get("line").get("name"),
						network=schedule.get("line").get("network"),
						shortName=schedule.get("line").get("shortName"),
						style=schedule.get("line").get("style")
					)

					stop = Stop.Stop(
						handicappedCompliance=schedule.get("stop").get("handicappedCompliance"),
						id=int(schedule.get("stop").get("id")),
						name=schedule.get("stop").get("name"),
						operatorCode=API.toIntArray(schedule.get("stop").get("operatorCode"))
					)

					schedules.append(Schedule.Schedule(
						destination=destination,
						journeys=journeys,
						line=line,
						stop=stop
					))

				stopAreas.append(StopArea.StopArea(
					cityId=int(stopArea.get("cityId")),
					cityName=stopArea.get("cityName"),
					id=int(stopArea.get("id")),
					name=stopArea.get("name"),
					schedules=schedules,
					uniqueStopId=None if stopArea.get("uniqueStopId") == None else int(stopArea.get("uniqueStopId"))
				))

		return Departures.Departures(
			departures,
			stop,
			stopArea,
			stopAreas
		)

	def getRollingStocks(self):
		rollingStocks = []
		for rollingStock in self.request(service_name = "rolling_stocks", params={})["content"].get("rollingStocks"):
			rollingStocks.append(RollingStock.RollingStock(int(rollingStock.get("id")), rollingStock.get("name")))

		return rollingStocks

	def getJourneys(self):
		return None

	def getMessages(self, network=None, contentFormat="text", displayImportantOnly=False):
		if not API.isValidContentFormat(contentFormat):
			raise ValueError("Invalid contentFormat")

		params = {
			"network": network,
			"contentFormat": contentFormat,
			"displayImportantOnly": displayImportantOnly
		}

		messages = []
		for message in self.request(service_name = "messages", params = API.validateParams(params))["content"].get("messages"):
			message = message.get("message")
			messages.append(Message.Message(
				message.get("content"),
				message.get("id"),
				message.get("importanceLevel"),
				message.get("scope"),
				message.get("title"),
				message.get("type"),
				message.get("url")
			))

		return messages

	def getServicesDensity(self, centerXY=None, srid=4326, serviceNumber=10, beginDateTimeSlot=None, endDateTimeSlot=None, networkList=6192449487677451, rollingStockList=None, displayServices=False, displayResultTable=False, mad=None):
		if not API.isValidXY(centerXY):
			raise ValueError("Invalid centerXY")

		params = {
			"centerXY": centerXY if centerXY == None else "+".join(map(str, centerXY)),
			"srid": srid,
			"serviceNumber": serviceNumber,
			"beginDateTimeSlot": beginDateTimeSlot,
			"endDateTimeSlot": endDateTimeSlot,
			"networkList": networkList,
			"rollingStockList": rollingStockList if rollingStockList == None else ",".join(map(str, rollingStockList)),
			"displayServices": displayServices,
			"displayResultTable": displayResultTable,
			"mad": mad
		}

		servicesDensity = self.request(service_name = "services_density", params = API.validateParams(params))["content"]
		if servicesDensity == None:
			return None
		else:
			stops = []
			for stop in servicesDensity.get("stops"):
				services = []
				for service in stop.get("services"):
					services.append(Service.Service(
						service.get("JourneyName"),
						service.get("LineName"),
						service.get("departure"),
						service.get("id"),
						service.get("network"),
						service.get("rollingStock")
					))

				stops.append(Stop.Stop(
					stop.get("dataSource"),
					stop.get("distance"),
					stop.get("id"),
					stop.get("name"),
					services
				))

			return ServicesDensity.ServicesDensity(
				int(servicesDensity.get("distance")),
				bool(int(servicesDensity.get("isServiceNumberReached"))),
				int(servicesDensity.get("serviceNumberReached")),
				stops
			)

