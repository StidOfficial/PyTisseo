class Message:
	def __init__(self, content, id, importanceLevel, scope, title, type, url):
		self.content = content
		self.id = id
		self.importanceLevel = importanceLevel
		self.scope = scope
		self.title = title
		self.type = type
		self.url = url

	def __str__(self):
		return ("{{content: \"{}\", id: {}, importanceLevel: \"{}\", scope: \"{}\", "
			"title: \"{}\, type: \"{}\", url: \"{}\"}}").format(self.content, self.id,
				self.importanceLevel, self.scope, self.title, self.type, self.url)
