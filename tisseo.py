import Tisseo.API as Tisseo
import os
from tkinter import *
import datetime

Tisseo = Tisseo.API(protocol="http", key=os.getenv("TISSEO_API_KEY"))

for network in Tisseo.getNetworks():
	print("Network ID:", network.id)
	print("Network name:", network.name)

jeanJauresLine = Tisseo.getLine(shortName="A")
stopAreaId = Tisseo.getStopArea(lineId=jeanJauresLine.id, name="Jean Jaurès").id

class TisseoDepartures():
	def __init__(self, master=None):
		self.master = master
		self.master.title("Tisseo departures")
		self.master.attributes("-zoomed", True)
		self.master.attributes("-fullscreen", True)
		self.master.bind("<Escape>", self.quit)

		self.blink = False
		self.updateBlink()

		self.handicapIcon = PhotoImage(file="img/handicap.png")

		self.canvas = Canvas(master, width=1920, height=1080)
		self.canvas.pack()
		self.canvas.update()

		self.createElements()
		self.update()

	def quit(self, event=None):
		self.master.quit()

	def updateBlink(self):
		self.blink = not self.blink
		self.master.after(1000, self.updateBlink)

	def update(self):
		i = 0
		stopArea = Tisseo.getStopsSchedules(stopAreaId=stopAreaId, timetableByArea=True, number=1).stopAreas[0]

		self.canvas.itemconfig(self.title_id, text="{} - {}".format(stopArea.cityName, stopArea.name))

		for i in range(len(stopArea.schedules)):
			for a in range(len(stopArea.schedules)):
				if stopArea.schedules[i].journeys[0].waitingTime < stopArea.schedules[a].journeys[0].waitingTime:
					stopArea.schedules.insert(a, stopArea.schedules.pop(i))

		if self.blink:
			blinkColor = "white"
		else:
			blinkColor = "black"

		for i in range(len(stopArea.schedules)):
			if i < len(self.elements):
				schedule = stopArea.schedules[i]
				journey = schedule.journeys[0]

				if journey.getWaitingTime() < datetime.time(0, 1):
					self.canvas.itemconfig(self.elements[i]["line_panel_id"], fill="#FF8080")
					self.canvas.itemconfig(self.elements[i]["line_name_id"], fill=blinkColor)
					self.canvas.itemconfig(self.elements[i]["line_wating_time_id"], fill=blinkColor)
				else:
					self.canvas.itemconfig(self.elements[i]["line_panel_id"], fill="#FFFFFF")
					self.canvas.itemconfig(self.elements[i]["line_name_id"], fill="black")
					self.canvas.itemconfig(self.elements[i]["line_wating_time_id"], fill="black")

				self.canvas.itemconfig(self.elements[i]["line_transportmode_letter_id"], text=Tisseo.getLine(shortName=schedule.line.shortName).transportMode.getShortName())
				self.canvas.itemconfig(self.elements[i]["line_shortname_panel_id"], fill=schedule.line.bgXmlColor)
				self.canvas.itemconfig(self.elements[i]["line_shortname_id"], text=schedule.line.shortName, fill=schedule.line.fgXmlColor)
				self.canvas.itemconfig(self.elements[i]["line_name_id"], text=schedule.stop.name + " ➤ " + schedule.destination.name)
				if schedule.stop.handicappedCompliance:
					self.canvas.itemconfig(self.elements[i]["line_handicappedcompliance_id"], state="normal")
				else:
					self.canvas.itemconfig(self.elements[i]["line_handicappedcompliance_id"], state="hidden")

				self.canvas.itemconfig(self.elements[i]["line_wating_time_id"], text=("~ " if not journey.realTime else "") + str(journey.getWaitingTime()))

		self.canvas.itemconfig(self.clock_id, text=datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

		self.master.after(1000, self.update)

	def createElements(self):
		self.elements = []

		self.top_panel_id = self.canvas.create_rectangle(0, 0, self.canvas.winfo_width(), 60, outline="", fill="#2C3D53")
		self.title_id = self.canvas.create_text(self.canvas.winfo_width() / 2, (60 / 2), text="UNKNOWN - UNKNOWN", font="{Helvetica} 24 bold", fill="white")

		height = 80
		offsetY = 60
		for i in range(int(self.canvas.winfo_height() / height) - 1):
			self.elements.append({
				"line_transportmode_panel_id": self.canvas.create_rectangle(0, offsetY, height, offsetY + height, fill="#FFFFFF"),
				"line_transportmode_id": self.canvas.create_oval(10, offsetY + 10, height - 10, offsetY + height - 10, width=7, outline="#2c3589"),
				"line_transportmode_letter_id": self.canvas.create_text(height / 2, offsetY + (height / 2), text="-", font="{Helvetica Neue} 28 bold", fill="#2c3589"),
				"line_shortname_panel_id": self.canvas.create_rectangle(height + height, offsetY, height, offsetY + height, fill="red"),
				"line_shortname_id": self.canvas.create_text(height + height / 2, offsetY + (height / 2), text="NULL", font="{Helvetica Neue} 18 bold", fill="#FFFFFF"),
				"line_panel_id": self.canvas.create_rectangle(height + height, offsetY, self.canvas.winfo_width(), offsetY + height, fill="#FFFFFF"),
				"line_name_id": self.canvas.create_text(height + height + 20, offsetY + (height / 2), text="Text", font="{Helvetica Neue} 20", anchor=W, fill="#FFFFFF"),
				"line_handicappedcompliance_id": self.canvas.create_image(self.canvas.winfo_width() - 210, offsetY + (height / 2), image=self.handicapIcon),
				"line_wating_time_id": self.canvas.create_text(self.canvas.winfo_width() - 20, offsetY + (height / 2), text="00:00:00", font="{Helvetica Neue} 20", anchor=E, fill="#000000")
			})
			offsetY += height

		self.bottom_panel_id = self.canvas.create_rectangle(0, self.canvas.winfo_height() - 60, self.canvas.winfo_width(), self.canvas.winfo_height(), outline="", fill="#2C3D53")
		self.clock_id = self.canvas.create_text(self.canvas.winfo_width() - 10, (self.canvas.winfo_height() - (60 / 2)), text="00/00/2018 00:00:00", font="{Helvetica Neue} 20 bold", anchor=E, fill="white")

master = Tk()

tisseoDepartures = TisseoDepartures(master)
master.mainloop()
